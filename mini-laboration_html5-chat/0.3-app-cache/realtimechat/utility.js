/**
 * utility.js
 */

var crypto = require('crypto');
var responder = require('../responder');

function generateRandStr(length, charSet) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randStr = '';
    for (var charSetLength = charSet.length; --length; ) {
        var randIndex = Math.floor(Math.random() * charSetLength);
        randStr += charSet[randIndex];
    }
    return randStr;
}

function createMessageHash(UUID, message) {
    var HMAC = crypto.createHmac('md5', UUID);
    HMAC.update(message);

    return HMAC.digest('base64');
}

function sendErrorResponse(err, response) {
    var error = err.message.split('::');
    responder.respond(response, 200, {
        'headers': { 'Content-Type': 'application/json' },
        'content': JSON.stringify({
            status: parseInt(error[0]),
            data: [],
            errors: [ error[1] ]
        })
    });
}

function sendDataResponse(data, response) {
    responder.respond(response, 200, {
        'headers': { 'Content-Type': 'application/json' },
        'content': JSON.stringify({
            data: data
        })
    });
}

exports.createMessageHash = createMessageHash;
exports.generateRandStr = generateRandStr;
exports.sendErrorResponse = sendErrorResponse;
exports.sendDataResponse = sendDataResponse;
