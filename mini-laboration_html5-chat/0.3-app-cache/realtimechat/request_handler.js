/**
 * request_handler.js
 */

var user = require('./user');
var message = require('./message');
var utility = require('./utility');

function connectChatSession(data, response) {

    // TODO(mats.blomdahl@gmail.com): Verify input format.
    try {
        var username = data.data.userDetails.username;
        var refUserDetails = user.connect(username);

        console.log("Connected user " + refUserDetails.username + " (ID: '" + refUserDetails.UUID + "')");

        var messages = message.get();
        if (messages.length) {
            user.setProperty(refUserDetails.UUID, {
                lastMessageHash: messages[messages.length - 1].hash
            });
        }

        utility.sendDataResponse({
            userDetails: refUserDetails,
            messages: messages
        }, response);

    } catch (err) {
        utility.sendErrorResponse(err, response);
    }

}

function pushChatUpdate(data, response) {

    try {
        //console.log('data: '+JSON.stringify(data));

        var userDetails = data.data.userDetails;
        var messages = data.data.messages;
        user.verify(userDetails);

        var firstConnectedTimeStamp = user.getProperty(userDetails.UUID, 'timeStamps.firstConnected');

        var refMessages = message.add(userDetails.UUID, userDetails.username, firstConnectedTimeStamp, messages);

        console.log("Accepted " + refMessages.length + " message(s) from " + userDetails.username + " (ID: '" + userDetails.UUID + "')");

        utility.sendDataResponse({
            userDetails: userDetails,
            messages: refMessages
        }, response);

    } catch (err) {
        utility.sendErrorResponse(err, response);
    }

}

function pullChatUpdate(data, response) {

    try {
        //console.log('data: '+JSON.stringify(data));

        var userDetails = data.data.userDetails;
        user.verify(userDetails);

        var messages = message.get(userDetails.lastMessageHash, userDetails.UUID);

        if (messages.length) {
            user.setProperty(userDetails.UUID, {
                lastMessageHash: messages[messages.length - 1].hash
            });
        }

        utility.sendDataResponse({
            userDetails: userDetails,
            messages: messages
        }, response);

    } catch (err) {
        utility.sendErrorResponse(err, response);
    }

}

function disconnectChatSession(data, response) {

    try {
        var userDetails = data.data.userDetails;
        user.verify(userDetails);

        user.disconnect(userDetails.UUID);

        utility.sendDataResponse({
            userDetails: userDetails
        }, response);

    } catch (err) {
        utility.sendErrorResponse(err, response);
    }

}

exports.connectChatSession = connectChatSession;
exports.pushChatUpdate = pushChatUpdate;
exports.pullChatUpdate = pullChatUpdate;
exports.disconnectChatSession = disconnectChatSession;
