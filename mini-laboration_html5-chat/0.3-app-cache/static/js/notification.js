/**
 * notification.js
 */

var Notification = (function() {

    // Konfiguration – minsta intervall mellan meddelanden (ms)
    var MIN_RENDER_INTERVALL = 5e2;

    // Konfiguration – default-värden för nya meddelanden
    var DEFAULTS = {
        message: '',
        color: '#333',
        backgroundColor: '#ccc',
        removeButton: true,
        timeout: 6e3,
        onRenderCallback: function() {
            console.debug('Notification rendered.');
        },
        onCloseCallback: function() {
            console.debug('Notification closed.');
        }
    };

    // Klassvariabler
    var wrapperSelector = '.notificationview';
    var wrapperEl;
    var lastRenderTimestamp;

    // Funktion som genererar ett element för vårt meddelande
    function generateNotification(label, message, color, backgroundColor, removeButton) {
        var wrapperDiv = document.createElement('div');
        var labelSpan = document.createElement('span');
        var messageSpan = document.createElement('span');

        wrapperDiv.style.backgroundColor = backgroundColor;

        labelSpan.style.fontWeight = 700;
        labelSpan.style.color = color;
        labelSpan.innerHTML = label;
        wrapperDiv.appendChild(labelSpan);

        messageSpan.style.color = color;
        messageSpan.innerHTML = message;
        wrapperDiv.appendChild(messageSpan);

        if (removeButton) {
            removeButton = document.createElement('button');
            removeButton.innerText = 'X';
            removeButton.onclick = function() {
                fireEvent.call(wrapperDiv, 'notificationremoved', { });
                wrapperEl.removeChild(wrapperDiv);
            };
            wrapperDiv.appendChild(removeButton);
        }

        return wrapperDiv;
    }

    // Funktion som binder variabeln ´´wrapperEl´´ till vår ´´notificationview´´-sektion.
    function bindNotificationView(callback) {
        wrapperEl = document.querySelector(wrapperSelector);
        if (wrapperEl) {
            return callback();
        } else {
            document.addEventListener('readystatechange', function () {
                if (document.readyState === "complete") {
                    callback();
                }
            });
        }
    }

    // Ansvarar för att rendera meddelandet i DOM:en och anropa callback.
    function renderNotification(notification, timeout, onRenderCallback, onCloseCallback) {
        var renderDelay = 0;
        var timeoutHandle;

        // Ser till att vi inte renderar meddelanden snabbare än ´´MIN_RENDER_INTERVALL´´
        if (lastRenderTimestamp) {
            //console.debug('old lastRenderTimestamp: '+lastRenderTimestamp);
            var now = new Date();
            if (now <= lastRenderTimestamp) {
                lastRenderTimestamp = new Date(lastRenderTimestamp.valueOf() + MIN_RENDER_INTERVALL);
                renderDelay = lastRenderTimestamp - now;
            } else {
                renderDelay = Math.abs(lastRenderTimestamp - now);
                //console.debug('dt(renderDelay): '+renderDelay);
                renderDelay = renderDelay < MIN_RENDER_INTERVALL ? MIN_RENDER_INTERVALL - renderDelay : 0;
                lastRenderTimestamp = new Date(now.valueOf() + renderDelay);
            }

            //console.debug('updated renderDelay: '+renderDelay);

            //console.debug('new lastRenderTimestamp 1: '+lastRenderTimestamp);

        } else {
            lastRenderTimestamp = new Date();
            //console.debug('new lastRenderTimestamp 2: '+lastRenderTimestamp);
        }

        setTimeout(function() {

            // tar bort vårt meddelande efter ´´timeout´´ ms
            if (timeout) {
                timeoutHandle = setTimeout(function() {
                    wrapperEl.removeChild(notification);
                    onCloseCallback();
                }, timeout);
            }

            // rensar upp automatisk timeout ifall användaren klickar bort meddelandet
            notification.addEventListener('notificationremoved', function() {
                if (timeoutHandle) {
                    clearTimeout(timeoutHandle);
                    onCloseCallback();
                }
            });

            //console.debug('Notification rendered (delay: ' + renderDelay + ' ms).');
            wrapperEl.appendChild(notification);
            onRenderCallback();

        }, renderDelay);
    }

    console.debug(new Date() + ': Notification prototype initialized.');

    // Vårt publika gränssnitt, tilldelas ´´Notification´´-objektet i vårt globala scope.
    return function(options, callback) {
        var label = options.label || DEFAULTS.label;
        var message = options.message || DEFAULTS.message;
        var color = options.color || DEFAULTS.color;
        var backgroundColor = options.backgroundColor || DEFAULTS.backgroundColor;

        var removeButton = DEFAULTS.removeButton;
        if (typeof options.removeButton === 'boolean') {
            removeButton = options.removeButton;
        }

        var timeout = DEFAULTS.timeout;
        if (typeof options.timeout === 'number') {
            timeout = options.timeout;
        }

        var onCloseCallback = options.onCloseCallback || DEFAULTS.onCloseCallback;
        var onRenderCallback = options.onRenderCallback || DEFAULTS.onRenderCallback;

        var notification = generateNotification(label, message, color, backgroundColor, removeButton);

        if (!wrapperEl) {
            bindNotificationView(function() {
                renderNotification(notification, timeout, onRenderCallback, onCloseCallback);
            });
        } else {
            renderNotification(notification, timeout, onRenderCallback, onCloseCallback);
        }

        return this;
    };

})();


