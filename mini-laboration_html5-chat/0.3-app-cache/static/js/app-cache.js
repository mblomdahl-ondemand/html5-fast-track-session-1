/**
 * app-cache.js
 */

var AppCache = new function() {

    var cache = window.applicationCache;

    var currentEvents = [];

    // Private helper
    function getStatus() {
        // Cleartext AppCache status:
        // http://www.html5rocks.com/en/tutorials/appcache/beginner/#toc-updating-cache
        switch (cache.status) {
            case cache.UNCACHED: // UNCACHED == 0
                return 'UNCACHED';
                break;
            case cache.IDLE: // IDLE == 1
                return 'IDLE';
                break;
            case cache.CHECKING: // CHECKING == 2
                return 'CHECKING';
                break;
            case cache.DOWNLOADING: // DOWNLOADING == 3
                return 'DOWNLOADING';
                break;
            case cache.UPDATEREADY:  // UPDATEREADY == 4
                return 'UPDATEREADY';
                break;
            case cache.OBSOLETE: // OBSOLETE == 5
                return 'OBSOLETE';
                break;
            default:
                return 'UKNOWN CACHE STATUS';
                break;
        }
    }

    function displayStatus(cacheEvent) {
        var status = getStatus();

        if (currentEvents.indexOf(status) === -1) {
            currentEvents.push(status);
            new Notification({
                label: 'AppCache',
                message: '"' + status + '" event triggered',
                color: 'blue',
                removeButton: true,
                timeout: 5e3,
                onCloseCallback: function() {
                    //console.debug('"' + status + '" event message cleared');
                    currentEvents.splice(currentEvents.indexOf(status), 1);
                },
                onRenderCallback: function() {
                    console.debug('"' + status + '" event message rendered');
                }
            });
        }

        if (cache.status == cache.UPDATEREADY) {
            cache.swapCache();
            new Notification({
                label: 'Uppdaterad',
                message: 'En uppdaterad version av webbappen finns tillgänglig! <a href="javascript:window.location.reload();">Jag vill uppdatera nu!</a>',
                color: '#333',
                backgroundColor: 'yellow',
                removeButton: true,
                timeout: 2e4
            });
        }
    }

    // Fired after the first cache of the manifest.
    cache.addEventListener('cached', displayStatus, false);

    // Checking for an update. Always the first event fired in the sequence.
    cache.addEventListener('checking', displayStatus, false);

    // An update was found. The browser is fetching resources.
    cache.addEventListener('downloading', displayStatus, false);

    // The manifest returns 404 or 410, the download failed,
    // or the manifest changed while the download was in progress.
    cache.addEventListener('error', displayStatus, false);

    // Fired after the first download of the manifest.
    cache.addEventListener('noupdate', displayStatus, false);

    // Fired if the manifest file returns a 404 or 410.
    // This results in the application cache being deleted.
    cache.addEventListener('obsolete', displayStatus, false);

    // Fired for each resource listed in the manifest as it is being fetched.
    cache.addEventListener('progress', displayStatus, false);

    // Fired when the manifest resources have been newly redownloaded.
    cache.addEventListener('updateready', displayStatus, false);

    console.debug(new Date() + ': AppCache singleton initialized.');
};
