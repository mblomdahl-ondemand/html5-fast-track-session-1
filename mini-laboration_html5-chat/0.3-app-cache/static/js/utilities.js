/**
 * utilities.js
 */

// Event firing, simplified
function fireEvent(name, data) {
    var targetEl = this instanceof HTMLElement ? this : document;
    console.log('targetEl:');
    console.log(targetEl);
    var e = document.createEvent("Event");
    e.initEvent(name, true, true); // eventName, canBubble, cancelable
    e.data = data;
    targetEl.dispatchEvent(e);
}
