/**
 * app.js
 */

var server = require('./server');

var router = require('./router');

var request_handler = require('./request_handler');

var handle = {};
handle['/'] = request_handler.foo;
handle['/foo'] = request_handler.foo;
handle['/bar'] = request_handler.bar;

// Realtime chat implementation
handle['/chat/connect'] = request_handler.connectChatSession;
handle['/chat/push_update'] = request_handler.pushChatUpdate;
handle['/chat/pull_update'] = request_handler.pullChatUpdate;
handle['/chat/disconnect'] = request_handler.disconnectChatSession;

server.start(router.route, handle);
