/**
 * user.js
 */

var utility = require('./utility');

var users = {};

var DISCONNECTED = 0;
var CONNECTED = 1;
var INACTIVE = 2;
var ACTIVE = 3;


function setProperty(UUID, propertyMap) {

    for (var property in propertyMap) {
        var splitProperty = property.split('.');
        if (splitProperty instanceof Array && splitProperty.length == 2) {
            users[UUID][splitProperty[0]][splitProperty[1]] = propertyMap[property];
            // console.log('updated user '+UUID+' /at "['+splitProperty[0]+']['+splitProperty[1]+'" to value '+propertyMap[property]);
        } else {
            users[UUID][property] = propertyMap[property];
            // console.log('updated user '+UUID+' /at "['+property+'] to value '+propertyMap[property]);
        }
    }
}

function getProperty(UUID, property) {

    var splitProperty = property.split('.');
    if (splitProperty instanceof Array && splitProperty.length == 2) {
        // console.log('read from user '+UUID+' /at "['+splitProperty[0]+']['+splitProperty[1]+'", returned '+users[UUID][splitProperty[0]][splitProperty[1]]);
        return users[UUID][splitProperty[0]][splitProperty[1]];
    } else {
        // console.log('read from user '+UUID+' /at "['+property+'], returned value '+users[UUID][property]);
        return users[UUID][property];
    }
}

function connect(username) {

    // TODO(mats.blomdahl@gmail.com): Verify username validity.

    var timeStamp = (new Date()).valueOf();
    var UUID = utility.generateRandStr(8);
    while (users[UUID]) {
        UUID = utility.generateRandStr(8);
    }

    users[UUID] = {
        username: username,
        UUID: UUID,
        state: CONNECTED,
        lastMessageHash: undefined,
        timeStamps: {
            lastInteraction: timeStamp,
            firstConnected: timeStamp,
            lastConnected: timeStamp,
            disconnected: undefined
        }
    };

    return users[UUID];
}

function disconnect(UUID) {

}

function verify(userDetails) {

    // TODO(mats.blomdahl@gmail.com): Verify username validity.

    if (!(userDetails.UUID && userDetails.username)) {
        throw new Error("400::Bad Request (Identifiers missing)");
    }
    if (!users[userDetails.UUID]) {
        throw new Error("404::Not Found (Unidentified UUID)")
    }
    if (users[userDetails.UUID].username !== userDetails.username) {
        throw new Error("403::Forbidden (Forged username)");
    }
    if (!users[userDetails.UUID].state) {
        throw new Error("401::Unauthorized (Not connected)");
    }

    setProperty(userDetails.UUID, { 'timeStamps.lastInteraction': (new Date()).valueOf() });

    return true;
}

exports.connect = connect;
exports.setProperty = setProperty;
exports.getProperty = getProperty;
exports.disconnect = disconnect;
exports.verify = verify;
