/**
 * message.js
 */

var utility = require('./utility');

// An array – the perfect replacement for some comlicated database integration
var messageHistory = [];

// Settings
var HISTORY_MAX_LENGTH = 200;

function add(UUID, username, firstConnectedTimeStamp, messages) {

    function verifyOrigin(message) {
        if (message.UUID !== UUID || message.username !== username) {
            throw new Error("403::Bad Request (Forged identity)");
        }
        if (message.timeStamp < firstConnectedTimeStamp) {
            throw new Error("403::Bad Request (Forged timestamp)");
        }
    }

    messages.forEach(function(message, index, messages) {
        verifyOrigin(message);
        messages[index].hash = utility.createMessageHash(message.UUID, JSON.stringify(message));
    });

    messages.forEach(function(message, index, messages) {
        messageHistory.push(message);
        if (messageHistory.length > HISTORY_MAX_LENGTH) {
            messageHistory.shift();
        }
    });

    return messages;
}

function get(lastMessageHash, excludeUUID) {
    if (lastMessageHash || excludeUUID) {
        var messages = [];

        for (var i = messageHistory.length; i--; ) {
            if (messageHistory[i].hash !== lastMessageHash) {

                // console.log('messageHistory[i].hash:'+messageHistory[i].hash);
                // console.log('lastMessageHash:'+lastMessageHash);

                if (messageHistory[i].UUID !== excludeUUID) {
                    messages.unshift(messageHistory[i]);
                }

            } else {
                break;
            }
        }
        return messages;

    } else {
        return messageHistory;
    }
}

exports.add = add;
exports.get = get;
