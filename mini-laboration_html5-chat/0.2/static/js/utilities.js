/**
 * utilities.js
 */

// Event firing, simplified
function fireEvent(name, data) {
    var e = document.createEvent("Event");
    e.initEvent(name, true, true); // eventName, canBubble, cancelable
    e.data = data;
    document.dispatchEvent(e);
}
