/**
 * chat-client.js
 */

var ChatClient = new function() {

    // Connection states
    var STATUS_ALIVE = 'CONNECTION_ALIVE';
    var STATUS_AVAILABLE = 'CONNECTION_AVAILABLE';
    var STATUS_OFFLINE = 'CONNECTION_OFFLINE';

    // Message history limits
    var MAX_HISTORY = 400;
    var TRIM_THRESHOLD = 380;

    // Connection settings
    var POLLING_INTERVAL = 5e2;
    var CONNECTION_TIMEOUT = 5e3;

    // Session state
    function generateDefaultSession_() {
        return {
            UUID: undefined,
            username: undefined,
            lastMessageHash: undefined,
            timeStamps: {
                firstConnected: undefined,
                lastInteraction: undefined,
                lastConnected: undefined,
                disconnected: undefined
            },
            connectionStatus: STATUS_AVAILABLE,
            pollingTimeout: undefined
        };
    }
    var sessionDetails = generateDefaultSession_();

    // DOM interface
    var DomInterface = new function() {

        // Message container element
        var messageView;

        // Private
        function trimMessageView_() {
            if (Math.random() > 0.25) {
                return;
            }
            var messages = document.querySelectorAll('.messageview > div > p');
            if (messages.length < MAX_HISTORY) {
                return;
            }
            var removableParentWrappers = [];
            var removableMessages = [];
            for (var i = 0, j = messages.length - TRIM_THRESHOLD; i < j; i++) {
                removableMessages.push(messages[i]);
                if (removableParentWrappers.indexOf(messages[i].parentNode) === -1) {
                    removableParentWrappers.push(messages[i].parentNode);
                }
            }
            console.log('found ' + removableMessages.length + ' removable messages.');
            console.log('found ' + removableParentWrappers.length + ' removable parent wrappers.');
            removableParentWrappers.forEach(function(wrapper) {
                var messages = wrapper.querySelectorAll('p');
                var removable = true;
                for (var i = 0, j = messages.length; i < j; i++) {
                    if (removableMessages.indexOf(messages[i]) === -1) {
                        removable = false;
                    }
                }
                if (removable) {
                    for (var i = 0, j = messages.length; i < j; i++) {
                        removableMessages.splice(removableMessages.indexOf(messages[i]), 1);
                        console.debug('removed message '+messages[i].getAttribute('data-hash'));
                    }
                    wrapper.parentNode.removeChild(wrapper);
                    console.debug('removed wrapper '+wrapper.getAttribute('data-uuid'));
                }
            });
            console.log('found ' + removableMessages.length + ' remaining removables.');
            removableMessages.forEach(function(message) {
                console.log('removing p and time '+message.id);
                message.parentNode.removeChild(message);
            });
        }
        window.addEventListener('chatMessageAppended', trimMessageView_, true);

        // Private
        function newMessageEl_(username, UUID, timestamp, message, hash) {
            var wrapperEl = document.createElement('div');
            wrapperEl.setAttribute('data-uuid', UUID);
            wrapperEl.className = 'message';
            wrapperEl.innerHTML = [ "<header><h4>", username, " skriver: </h4></header>" ].join('');
            return updateMessageEl_(wrapperEl, timestamp, message, hash);
        }

        // Private
        function updateMessageEl_(wrapperEl, timestamp, message, hash) {
            var pEl = document.createElement('p');
            var timeEl = document.createElement('time');
            var datetime = new Date(timestamp);
            timeEl.dateTime = datetime.toISOString();
            timeEl.innerText = datetime.toLocaleTimeString('sv');
            if (hash) {
                pEl.setAttribute('data-hash', hash);
                timeEl.setAttribute('data-hash', hash);
            }
            pEl.appendChild(timeEl);
            pEl.appendChild(document.createTextNode(message));
            wrapperEl.appendChild(pEl);
            return wrapperEl;
        }

        // Public interface
        this.appendMessage = function(username, UUID, timestamp, message, hash) {
            messageView = messageView || document.querySelector('.messageview');
            var lastMessage = messageView.querySelector('div:last-child');
            if (lastMessage && UUID === lastMessage.getAttribute('data-uuid')) {
                updateMessageEl_(lastMessage, timestamp, message, hash);
            } else {
                lastMessage = newMessageEl_(username, UUID, timestamp, message, hash);
                messageView.appendChild(lastMessage);
            }
            lastMessage.querySelector('p:last-child').scrollIntoView();
            fireEvent('chatMessageAppended');
        };

    };

    var ServerInterface = new function() {

        // Private – JSON posts using XmlHttpRequest, simplified
        function submitXHR_(url, data, callback, timeout) {
            var maxWaitTime = timeout || CONNECTION_TIMEOUT;
            var xhr = new XMLHttpRequest();
            var noResponseTimer = setTimeout(function() {
                xhr.abort();
            }, maxWaitTime);
            xhr.onreadystatechange = function() {
                if (xhr.readyState != 4) {
                    return;
                }
                clearTimeout(noResponseTimer);
                callback(JSON.parse(xhr.responseText));
            };
            xhr.open("POST", url);
            xhr.send(JSON.stringify(data));
        }

        // Private
        function generateRequest_() {
            var timeStamp = (new Date()).valueOf();
            var data = {
                userDetails: {
                    UUID: sessionDetails.UUID,
                    username: sessionDetails.username,
                    timeStamps: sessionDetails.timeStamps,
                    lastMessageHash: sessionDetails.lastMessageHash
                },
                timeStamp: timeStamp
            };
            return data;
        }

        // Private
        function loadMessages_(messages) {
            messages.forEach(function(message) {
                DomInterface.appendMessage(message.username, message.UUID, message.timeStamp, message.message, message.hash);
            });
            if (messages.length) {
                sessionDetails.lastMessageHash = messages[messages.length - 1].hash;
            }
        }

        // Public interface
        this.establishConnection = function(username, callback) {
            sessionDetails.username = username;
            var self = this;
            submitXHR_('/chat/connect', generateRequest_(), function(data) {
                //console.log(data);
                var userDetails = data.data.userDetails;
                sessionDetails.timeStamps.firstConnected = userDetails.timeStamps.firstConnected;
                sessionDetails.username = userDetails.username;
                sessionDetails.UUID = userDetails.UUID;
                loadMessages_(data.data.messages);
                self.initiatePolling();
                if (callback) {
                    callback(false, data);
                }
                setTimeout(function() { chatTestPosting(); }, 1e3);
            });

        };

        // Public interface
        this.initiatePolling = function() {
            sessionDetails.connectionStatus = STATUS_ALIVE;
            sessionDetails.pollingTimeout = setTimeout(function() {
                submitXHR_('/chat/pull_update', generateRequest_(), function(data) {
                    if (sessionDetails.connectionStatus !== STATUS_ALIVE) {
                        return;
                    }
                    fireEvent('pollingOpCompleted', {});
                    loadMessages_(data.data.messages);
                });

            }, POLLING_INTERVAL);
        };
        window.addEventListener('pollingOpCompleted', this.initiatePolling, true);

        // Public interface
        this.suspendPolling = function() {
            clearTimeout(sessionDetails.pollingTimeout);
            sessionDetails.pollingTimeout = undefined;
        };

        // Public interface
        this.terminateConnection = function(callback) {
            submitXHR_('/chat/disconnect', generateRequest_(), function(data) {
                if (callback) {
                    callback(false, data);
                }
            });
        };

        // Public interface
        this.sendMessage = function(timeStamp, message, callback) {
            var data = generateRequest_();
            data.messages = [ {
                UUID: sessionDetails.UUID,
                username: sessionDetails.username,
                timeStamp: timeStamp,
                message: message
            } ];
            submitXHR_('/chat/push_update', data, function(data) {
                if (callback) {
                    callback(false, data);
                }
            });
        }

    };

    // Public interface
    this.submitMessage = function(message, callback) {
        var timeStamp = (new Date()).valueOf();
        ServerInterface.sendMessage(timeStamp, message, callback);
        DomInterface.appendMessage(sessionDetails.username, sessionDetails.UUID, timeStamp, message);
    };

    // Public interface
    this.connect = function(username, callback) {
        ServerInterface.establishConnection(username, callback)
    };

    // Public interface
    this.disconnect = function(callback) {
        if (sessionDetails.connection.status == STATUS_ALIVE) {
            ServerInterface.suspendPolling();
            ServerInterface.terminateConnection(callback);
            sessionDetails = generateDefaultSession_();
        } else {
            if (callback) {
                callback(true, null);
            }
        }
    };
};

// Debug / load testing
function chatTestPosting() {
    var dumbthings = ['Bla bla...', 'Bla bla bla bla bla.', 'Blablabla-bla-bla bla. Bla.', 'Blaha bla?', 'Foo bla, blaha!', '... bla ... bla'];
    setTimeout(function() {
        var testSeq = [];
        for (var i = 0, j = Math.floor(Math.random() * 20 + 1); i < j; i++) {
            testSeq.push(dumbthings[Math.floor(Math.random()*dumbthings.length)]);
        }
        testSeq = testSeq.join(' ');
        ChatClient.submitMessage(testSeq);
        fireEvent('chatTestPosting');
    }, 1000 + Math.random() * 3e3);
};
window.addEventListener('chatTestPosting', chatTestPosting, true);
