/**
 * server.js
 */

var http = require('http');

/**
 * Module ´url´
 *                               url.parse(string).query
 *                                           |
 *            url.parse(string).pathname     |
 *                        |                  |
 *                        |                  |
 *                      ------ -------------------
 * http://localhost:1337/start?foo=bar&hello=world
 *                             ---           -----
 *                              |              |
 *                              |              |
 *           querystring(string)["foo"]        |
 *                                             |
 *                          querystring(string)["hello"]
 */
var url = require('url');
var querystring = require('querystring');

function start(route, handle) {

    var httpServer = http.createServer(function(request, response) {

        var pathname = url.parse(request.url).pathname;
        console.log("Request for " + pathname + " received.");

        var query = url.parse(request.url).query;
        var payload = {};

        if (query) {
            payload['data'] = querystring.parse(query);
            // console.error('query: ' + payload['data']);
        }

        if (request.method == 'POST') {
            var body = '';

            request.on('data', function(dataMsg) {
                body += dataMsg;
            });

            request.on('end', function () {
                // console.log('body:'+body);
                var bodyData;// = querystring.parse(body);

                bodyData = JSON.parse(body);

                // console.log('bodyData:'+bodyData);
                if (payload.data) {
                    for (var param in bodyData) {
                        // console.log('param:'+param);
                        payload.data[param] = bodyData[param];
                    }
                } else {
                    // console.log("payload['data'] = bodyData;");
                    payload['data'] = bodyData;
                }

                route(handle.http, pathname, payload, response);
            });

        } else {
            route(handle, pathname, payload, response);
        }
    });

    httpServer.listen(1337);

    console.log(new Date() + ": HTTP server has started.");

    return httpServer;
}

exports.start = start;
