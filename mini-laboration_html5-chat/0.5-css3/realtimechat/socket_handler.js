/**
 * socket_handler.js
 */

var user = require('./user');
var utility = require('./utility');

function clientConnect(socket) {
    socket.emit('client_connected', utility.wrapDataResponse({
        userDetails: {
            socketID: socket.id,
            username: undefined,
            UUID: undefined
        }
    }));
    socket.broadcast.emit('client_connected', utility.wrapDataResponse({ }));
}

function clientDisconnect(socket) {
    var chatUser = user.get(socket.id);
    var userData;
    if (chatUser) {
        userData = utility.wrapDataResponse({
            userDetails: chatUser.getPublicDetails()
        });
    } else {
        userData = utility.wrapDataResponse({});
    }
    socket.broadcast.emit('client_disconnected', userData);
}

function userLogin(socket, data) {
    //console.error('users.list:');
    //console.error(user.list());

    //try {
    var userDetails = data.data.userDetails;
    var chatUser = user.login(socket.id, userDetails);

    var fullUserDetails = chatUser.getFullDetails();
    var publicUserDetails = chatUser.getPublicDetails();

    socket.emit('user_login_completed', utility.wrapDataResponse({
        userDetails: fullUserDetails
    }));

    socket.broadcast.emit('user_login_completed', utility.wrapDataResponse({
        userDetails: publicUserDetails
    }));

    var messages = chatUser.getMessageHistory();

    socket.emit('message_history_retrieved', utility.wrapDataResponse({
        userDetails: chatUser.getFullDetails(),
        messages: messages
    }));

    //console.error('users.list:');
    //console.error(user.list());
    /*} catch (err) {
        socket.emit('error', utility.wrapErrorResponse(err));
    }*/

}

function userReconnect(socket, data) {
    //console.log('user_reconnect_request:');
    //console.log(socket);
    socket.broadcast.emit('user_reconnect_completed', utility.wrapDataResponse({
        //...
    }));

}

function userLogout(socket, data) {

    //console.log('user_logout data:');
    //console.log(data);

    //console.error('users.list:');
    //console.error(user.list());
    //try {
    var userDetails = data.data.userDetails;
    var chatUser = user.logout(socket.id, userDetails);

    socket.emit('user_logout_completed', utility.wrapDataResponse({
        userDetails: chatUser.getFullDetails()
    }));

    socket.broadcast.emit('user_logout_completed', utility.wrapDataResponse({
        userDetails: chatUser.getPublicDetails()
    }));

    /*} catch (err) {
        socket.emit('error', utility.wrapErrorResponse(err));
    }*/

}

function retrieveHistory(socket, data) {
    //console.log('retrieve_message_history:');
    //console.log(data);

    //try {
        //console.log('data: '+JSON.stringify(data));

    var userDetails = data.data.userDetails;
    var chatUser = user.get(socket.id, userDetails);
    var messages = chatUser.getMessageHistory();

    socket.emit('message_history_retrieved', utility.wrapDataResponse({
        userDetails: chatUser.getFullDetails(),
        messages: messages
    }));
    /*
    } catch (err) {
        socket.emit('error', utility.wrapErrorResponse(err));
    }*/

}

function submitMessage(socket, data) {

    //console.log('submit_message_request:');
    //console.log(data);

    //try {
        //console.log('data: '+JSON.stringify(data));

    var userDetails = data.data.userDetails;
    var messages = data.data.messages;
    var chatUser = user.get(socket.id, userDetails);

    messages = chatUser.addMessages(messages);

    socket.emit('user_message_submitted', utility.wrapDataResponse({
        userDetails: chatUser.getFullDetails(),
        messages: messages
    }));

    socket.broadcast.emit('user_message_submitted', utility.wrapDataResponse({
        userDetails: chatUser.getPublicDetails(),
        messages: messages
    }));

    /*} catch (err) {
        socket.emit('error', utility.wrapErrorResponse(err));
    }*/

}

function setup(chatSocket) {
    chatSocket.on('connection', function (socket) {

        clientConnect(socket);

        socket.on('disconnect', function(packet) {
            clientDisconnect(socket);
        });

        socket.on('user_login_request', function(data) {
            userLogin(socket, data);
        });

        socket.on('user_reconnect_request', function(data) {
            userReconnect(socket, data);
        });

        socket.on('user_logout_request', function(data) {
            userLogout(socket, data);
        });

        socket.on('retrieve_message_history', function(data) {
            retrieveHistory(socket, data);
        });

        socket.on('submit_message_request', function(data) {
            submitMessage(socket, data);
        });

    });

}

exports.setup = setup;
