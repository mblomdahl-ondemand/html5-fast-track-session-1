/**
 * socket.js
 */

var io = require('socket.io');

function start(httpServer, handle) {

    var socketServer = io.listen(httpServer);

    console.log(new Date() + ": WebSocket server has started.");

    for (var route in handle) {
        handle[route](socketServer.of(route));
        console.log(new Date() + ": Configured socket for route " + route + ".");
    }

}

exports.start = start;
