/**
 * script.js
 */

function bindUiControls() {

    document.getElementById('loginForm').onsubmit = function(submitEvent) {
        console.debug('#loginUsername submitted');

        ChatClient.connect(document.getElementById('usernameInput').value, function(success, message) {
            console.debug('Login Completed');
        });

        submitEvent.preventDefault();
        submitEvent.target.reset();
    };

    document.getElementById('messageForm').onsubmit = function(submitEvent) {
        console.debug('#messageInput submitted');

        ChatClient.submitMessage(document.getElementById('messageInput').value, function(success, message) {
            console.debug('Message submit completed');
        });

        submitEvent.preventDefault();
        submitEvent.target.reset();
    };

    document.getElementById('loginButton').onclick = function(clickEvent) {
        console.debug('#loginButton clicked');
        clickEvent.target.disabled = true;
    };

    document.getElementById('logoutButton').onclick = function(clickEvent) {
        console.debug('#logoutButton clicked');
        clickEvent.target.disabled = true;

        ChatClient.disconnect(function(success, message) {
            document.getElementById('loginButton').disabled = false;
            console.debug('Logout Completed');
        });
    };
}

document.onreadystatechange = function() {
    if (document.readyState === 'complete') {
        bindUiControls();
    }
};

