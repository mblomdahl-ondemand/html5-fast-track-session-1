/**
 * chat-client.js
 */

var ChatClient = new function() {

    // Connection states
    var STATUS_ALIVE = 'CONNECTION_ALIVE';
    var STATUS_AVAILABLE = 'CONNECTION_AVAILABLE';
    var STATUS_OFFLINE = 'CONNECTION_OFFLINE';

    // Message history limits
    var MAX_HISTORY = 400;
    var TRIM_THRESHOLD = 380;

    // Polling interval
    var POLLING_INTERVAL = 500;

    var sessionDetails = {
        UUID: undefined,
        username: undefined,
        lastMessageHash: undefined,
        connection: {
            startTime: undefined,
            duration: undefined,
            timeout: undefined,
            status: STATUS_AVAILABLE
        }
    };

    // Public interface
    this.submitMessage = function(message, callback) {
        /*...*/
    };

    // Public interface
    this.connect = function(username, callback) {
        /*...*/
    };

    // Public interface
    this.disconnect = function(callback) {
        /*...*/
    };

};
