/**
 * chat-client.js
 */

var ChatClient = new function() {

    // WebSocket connection states
    var STATUS_ALIVE = 'CONNECTION_ALIVE';
    var STATUS_AVAILABLE = 'CONNECTION_AVAILABLE';
    var STATUS_OFFLINE = 'CONNECTION_OFFLINE';

    // Message history limits
    var MAX_HISTORY = 400;
    var TRIM_THRESHOLD = MAX_HISTORY - 20;

    // Session state
    function generateDefaultSession_() {
        return {
            UUID: undefined,
            username: undefined,
            socketID: undefined,
            lastMessageHash: undefined,
            timeStamps: {
                firstConnected: undefined,
                lastInteraction: undefined,
                lastConnected: undefined,
                disconnected: undefined
            },
            connectionStatus: STATUS_AVAILABLE,
            pollingTimeout: undefined
        };
    }
    var sessionDetails = generateDefaultSession_();

    // Private DOM interface
    var DomInterface = new function() {

        // Message container element
        var messageView;

        // Private
        function trimMessageView_() {
            if (Math.random() > 0.25) {
                return;
            }
            var messages = document.querySelectorAll('.messageview > div > p');
            if (messages.length < MAX_HISTORY) {
                return;
            }
            var removableParentWrappers = [];
            var removableMessages = [];
            for (var i = 0, j = messages.length - TRIM_THRESHOLD; i < j; i++) {
                removableMessages.push(messages[i]);
                if (removableParentWrappers.indexOf(messages[i].parentNode) === -1) {
                    removableParentWrappers.push(messages[i].parentNode);
                }
            }
            console.log('found ' + removableMessages.length + ' removable messages.');
            console.log('found ' + removableParentWrappers.length + ' removable parent wrappers.');
            removableParentWrappers.forEach(function(wrapper) {
                var messages = wrapper.querySelectorAll('p');
                var removable = true;
                for (var i = 0, j = messages.length; i < j; i++) {
                    if (removableMessages.indexOf(messages[i]) === -1) {
                        removable = false;
                    }
                }
                if (removable) {
                    for (var i = 0, j = messages.length; i < j; i++) {
                        removableMessages.splice(removableMessages.indexOf(messages[i]), 1);
                        console.debug('removed message '+messages[i].getAttribute('data-hash'));
                    }
                    wrapper.parentNode.removeChild(wrapper);
                    console.debug('removed wrapper '+wrapper.getAttribute('data-uuid'));
                }
            });
            console.log('found ' + removableMessages.length + ' remaining removables.');
            removableMessages.forEach(function(message) {
                console.log('removing p and time '+message.id);
                message.parentNode.removeChild(message);
            });
        }
        window.addEventListener('chatMessageAppended', trimMessageView_, true);

        // Private
        function newMessageEl_(username, UUID, timestamp, message, hash) {
            var wrapperEl = document.createElement('div');
            wrapperEl.setAttribute('data-uuid', UUID);
            wrapperEl.className = 'message';
            wrapperEl.innerHTML = [ "<header><h4>", username, " skriver: </h4></header>" ].join('');
            return updateMessageEl_(wrapperEl, timestamp, message, hash);
        }

        // Private
        function updateMessageEl_(wrapperEl, timestamp, message, hash) {
            var pEl = document.createElement('p');
            var timeEl = document.createElement('time');
            var datetime = new Date(timestamp);
            timeEl.dateTime = datetime.toISOString();
            timeEl.innerText = datetime.toLocaleTimeString('sv');
            if (hash) {
                pEl.setAttribute('data-hash', hash);
                timeEl.setAttribute('data-hash', hash);
            }
            pEl.appendChild(timeEl);
            pEl.appendChild(document.createTextNode(message));
            wrapperEl.appendChild(pEl);
            return wrapperEl;
        }

        // Public interface
        this.appendMessage = function(username, UUID, timestamp, message, hash) {
            messageView = messageView || document.querySelector('.messageview');
            var lastMessage = messageView.querySelector('div:last-child');
            if (lastMessage && UUID === lastMessage.getAttribute('data-uuid')) {
                updateMessageEl_(lastMessage, timestamp, message, hash);
            } else {
                lastMessage = newMessageEl_(username, UUID, timestamp, message, hash);
                messageView.appendChild(lastMessage);
            }
            lastMessage.querySelector('p:last-child').scrollIntoView();
            fireEvent('chatMessageAppended');
        };

    };

    // Private server interface
    var ServerInterface = new function() {

        // TODO(mats.blomdahl@gmail.com): Make the socket handling suck less.
        var SocketWrapper = new function() {

            // Configuration object for inbound events
            var INBOUND_EVENT_CONFIG_ = {
                'client_connected': {
                    localEndHandler: function(data, userDetails) {
                        sessionDetails.connectionStatus = STATUS_ALIVE;
                        sessionDetails.socketID = userDetails.socketID;
                        displayStatus_("client_connected (local end) / id: " + sessionDetails.socketID);
                    },
                    remoteEndHandler: function() {
                        displayStatus_("client_connected (remote end)");
                    },
                    debug: true
                },
                'client_disconnected': {
                    localEndHandler: function() {
                        sessionDetails.connectionStatus = STATUS_AVAILABLE;
                        displayStatus_("client_disconnected (local end) / id: " + sessionDetails.socketID);
                    },
                    remoteEndHandler: function(data, userDetails) {
                        var message = "client_disconnected (remote end)";
                        if (userDetails) {
                            message += " / username: " + userDetails.username + " / uuid: " + userDetails.UUID;
                        }
                        displayStatus_(message);
                    },
                    debug: true
                },
                'user_login_completed': {
                    localEndHandler: function(data, userDetails) {
                        displayStatus_("user_login_completed (local end) / username: " + userDetails.username + " / uuid: " + userDetails.UUID);
                    },
                    remoteEndHandler: function(data, userDetails) {
                        displayStatus_("user_login_completed (remote end) / username: " + userDetails.username + " / uuid: " + userDetails.UUID);
                    },
                    debug: true
                },
                'user_reconnect_completed': {
                    localEndHandler: function(data, userDetails) {
                        displayStatus_("user_reconnect_completed (local end) / username: " + userDetails.username + " / uuid: " + userDetails.UUID);
                    },
                    remoteEndHandler: function(data, userDetails) {
                        displayStatus_("user_reconnect_completed (remote end) / username: " + userDetails.username + " / uuid: " + userDetails.UUID);
                    },
                    debug: true
                },
                'user_logout_completed': {
                    localEndHandler: function(data, userDetails) {
                        displayStatus_("user_logout_completed (local end) / username: " + userDetails.username + " / uuid: " + userDetails.UUID);
                    },
                    remoteEndHandler: function(data, userDetails) {
                        displayStatus_("user_logout_completed (remote end) / username: " + userDetails.username + " / uuid: " + userDetails.UUID);
                    },
                    debug: true
                },
                'user_message_submitted': {
                    localEndHandler: function(data, userDetails, messages) {
                        displayStatus_("user_message_submitted (local end) / username: " + userDetails.username + " / uuid: " + userDetails.UUID + " /  messages.length: " + messages.length);
                        loadMessages_(messages);
                    },
                    remoteEndHandler: function(data, userDetails, messages) {
                        displayStatus_("user_message_submitted (remote end) / username: " + userDetails.username + " / uuid: " + userDetails.UUID + " / messages.length: " + messages.length);
                        loadMessages_(messages);
                    },
                    debug: false
                },
                'system_message_submitted': {
                    localEndHandler: function(data, userDetails, messages) {
                        displayStatus_("system_message_submitted (local end) / username: " + userDetails.username + " / uuid: " + userDetails.UUID + " /  messages.length: " + messages.length);
                        loadMessages_(messages);
                    },
                    remoteEndHandler: function(data, userDetails, messages) {
                        displayStatus_("system_message_submitted (remote end) / username: " + userDetails.username + " / uuid: " + userDetails.UUID + " / messages.length: " + messages.length);
                        loadMessages_(messages);
                    },
                    debug: true
                },
                'message_history_retrieved': {
                    localEndHandler: function(data, userDetails, messages) {
                        displayStatus_("message_history_retrieved (local end) / messages.length: " + messages.length);
                        loadMessages_(messages);
                    },
                    remoteEndHandler: function(data, userDetails, messages) {
                        displayStatus_("message_history_retrieved (remote end)");
                    },
                    debug: true
                }
            };

            // Mapping of outbound to inbound events
            var OUTBOUND_EVENT_MAPPING_ = {
                'user_login_request': 'user_login_completed',
                'user_logout_request': 'user_logout_completed',
                'user_reconnect_request': 'user_reconnect_completed',
                'submit_message_request': 'user_message_submitted',
                'retrieve_message_history': 'message_history_retrieved'
            };

            // Socket.IO connector
            var socket_ = undefined;

            // Queued callbacks by inbound event
            var callbackQueue_ = (function() {
                var mapping = {};
                for (var event in INBOUND_EVENT_CONFIG_) {
                    mapping[event] = [];
                }
                return mapping;
            })();

            // Private
            function displayStatus_(message) {
                try {
                    new Notification({
                        label: "WebSocket Event: ",
                        message: message,
                        timeout: 6e3
                    });
                } catch (e) {
                    console.error(e.message);
                }
            }

            // Private
            function connect_(callback) {
                if (callback) {
                    callbackQueue_.client_connected = [callback];
                }

                function configureCallback_(event) {
                    //console.debug('configuring callback for event ' + event);
                    return function(data) {
                        if (INBOUND_EVENT_CONFIG_[event].debug) {
                            console.debug(new Date() + ": Inbound event \"" + event + "\":");
                            console.debug(data);
                        }

                        var userDetails = data.data.userDetails;
                        var messages = data.data.messages;

                        if (userDetails && userDetails.socketID == socket_.socket.sessionid) {
                            INBOUND_EVENT_CONFIG_[event].localEndHandler(data, userDetails, messages);
                            while (callbackQueue_[event].length) {
                                callbackQueue_[event].shift()(data);
                            }
                        } else {
                            INBOUND_EVENT_CONFIG_[event].remoteEndHandler(data, userDetails, messages);
                        }
                    }
                }

                socket_ = io.connect(window.location.origin + '/chat');

                for (var event in INBOUND_EVENT_CONFIG_) {
                    socket_.on(event, configureCallback_(event));
                }
            }

            // Public interface
            this.emit = function(event, data, callback) {
                if (!(socket_ && socket_.socket && socket_.socket.connected)) {
                    connect_(function() {
                        SocketWrapper.emit(event, data, callback);
                    });
                } else {
                    if (!(event in OUTBOUND_EVENT_MAPPING_)) {
                        throw new Error(new Date() + ': Undefined outbound event "'+event+'"');
                    }
                    if (callback) {
                        var inboundAnalogue = OUTBOUND_EVENT_MAPPING_[event];
                        callbackQueue_[inboundAnalogue].push(callback);
                    }
                    socket_.emit(event, data);

                    if (event !== 'submit_message_request') {
                        console.debug(new Date() + ': Emitted event "' + event + '":');
                        console.debug(data);
                    }
                }
            };

            // Public interface
            this.disconnect = function(callback) {
                if (socket_ && socket_.socket && socket_.socket.connected) {
                    socket_.disconnect(callback);
                } else {
                    callback();
                }
            };

        };

        // Private
        function generateRequest_(eOpts) {
            var timeStamp = new Date().valueOf();
            var data = {
                userDetails: {
                    UUID: sessionDetails.UUID,
                    username: sessionDetails.username,
                    socketID: sessionDetails.socketID,
                    lastMessageHash: sessionDetails.lastMessageHash
                }
            };
            if (eOpts) {
                for (var property in eOpts) {
                    data[property] = eOpts[property];
                }
            }
            return {
                data: data,
                errors: [],
                meta: {
                    originTimeStamp: timeStamp
                }
            };
        }

        // Private
        function loadMessages_(messages) {
            messages.forEach(function(message) {
                DomInterface.appendMessage(message.username, message.UUID, message.timeStamp, message.message, message.hash);
            });
            if (messages.length) {
                sessionDetails.lastMessageHash = messages[messages.length - 1].hash;
            }
        }

        // Public interface
        this.establishConnection = function(username, callback) {
            sessionDetails.username = username;

            SocketWrapper.emit('user_login_request', generateRequest_(), function(data) {
                var userDetails = data.data.userDetails;
                sessionDetails.UUID = userDetails.UUID;
                sessionDetails.username = userDetails.username;

                if (callback) {
                    callback(true, data);
                }
                // setTimeout(function() { chatTestPosting(); }, 1e3);
            });

        };

        // Public interface
        this.terminateConnection = function(callback) {
            SocketWrapper.emit('user_logout_request', generateRequest_(), function(data) {
                SocketWrapper.disconnect(function() {
                    if (callback) {
                        callback(true, data);
                    }
                });
            });
        };

        // Public interface
        this.submitMessage = function(timeStamp, message, callback) {
            var data = generateRequest_({
                messages: [ {
                    UUID: sessionDetails.UUID,
                    username: sessionDetails.username,
                    timeStamp: timeStamp,
                    message: message
                } ]
            });

            SocketWrapper.emit('submit_message_request', data, function(data) {
                if (callback) {
                    callback(true, data);
                }
            });
        }
    };

    // Public interface
    this.submitMessage = function(message, callback) {
        var timeStamp = new Date().valueOf();
        ServerInterface.submitMessage(timeStamp, message, callback);
    };

    // Public interface
    this.connect = function(username, callback) {
        ServerInterface.establishConnection(username, callback)
    };

    // Public interface
    this.disconnect = function(callback) {
        if (sessionDetails.connection.status == STATUS_ALIVE) {
            ServerInterface.suspendPolling();
            ServerInterface.terminateConnection(callback);
            sessionDetails = generateDefaultSession_();
        } else {
            if (callback) {
                callback(true, null);
            }
        }
    };

    console.debug(new Date() + ': ChatClient singleton initialized.');
};

// Debug / load testing
var stopTestPosting = false;
function chatTestPosting() {
    if (stopTestPosting) {
        stopTestPosting = false;
        return;
    }
    var dumbthings = ['Bla bla...', 'Bla bla bla bla bla.', 'Blablabla-bla-bla bla. Bla.', 'Blaha bla?', 'Foo bla, blaha!', '... bla ... bla'];
    setTimeout(function() {
        var testSeq = [];
        for (var i = 0, j = Math.floor(Math.random() * 20 + 1); i < j; i++) {
            testSeq.push(dumbthings[Math.floor(Math.random()*dumbthings.length)]);
        }
        testSeq = testSeq.join(' ');
        ChatClient.submitMessage(testSeq);
        fireEvent('chatTestPosting');
    }, 500 + Math.random() * 1e3);
}
window.addEventListener('chatTestPosting', chatTestPosting, true);
