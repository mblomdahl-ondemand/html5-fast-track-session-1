/**
 * message.js
 */

var utility = require('./utility');

// An array – the perfect replacement for some comlicated database integration
var messageHistory = [];

// Constants/settings
var HISTORY_MAX_LENGTH = 200;
var SYSTEM = 0;
var PUBLIC = 1;
var PRIVATE = 2;

function add(messages) {
    messages.forEach(function(message, index, messages) {
        messages[index].hash = utility.createMessageHash(message.UUID, JSON.stringify(message));
        messages[index].type = PUBLIC;
        messageHistory.push(messages[index]);
        if (messageHistory.length > HISTORY_MAX_LENGTH) {
            messageHistory.shift();
        }
    });

    return messages;
}

function get(UUID, lastMessageHash) {
    var messages = [];

    if (lastMessageHash) {
        for (var i = messageHistory.length; i--; ) {
            var message = messageHistory[i];

            if (message.hash !== lastMessageHash) {

                // console.log('messageHistory[i].hash:'+messageHistory[i].hash);
                // console.log('lastMessageHash:'+lastMessageHash);

                if (message.type < PRIVATE) {
                    messages.unshift(message);
                } else if (message.targetUUID === UUID || message.UUID === UUID) {
                    messages.unshift(message);
                }

            } else {
                break;
            }
        }
    } else {
        messageHistory.forEach(function(message) {
            if (message.type < PRIVATE) {
                messages.push(message);
            } else if (message.targetUUID === UUID || message.UUID === UUID) {
                messages.push(message);
            }
        });
    }

    return messages;
}

exports.add = add;
exports.get = get;
