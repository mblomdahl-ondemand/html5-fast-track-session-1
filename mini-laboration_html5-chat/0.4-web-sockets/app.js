/**
 * app.js
 */

var server = require('./server');
var socket = require('./socket');
var router = require('./router');
var request_handler = require('./request_handler');

var realtimechat = require('./realtimechat/socket_handler');

var httpHandle = {};
httpHandle['/'] = request_handler.foo;
httpHandle['/foo'] = request_handler.foo;
httpHandle['/bar'] = request_handler.bar;

var socketHandle = {};
socketHandle['/chat'] = realtimechat.setup;

socket.start(server.start(router.route, httpHandle), socketHandle);
