/**
 * server.js
 */

var http = require('http');

/**
 * Module ´url´
 *                               url.parse(string).query
 *                                           |
 *            url.parse(string).pathname     |
 *                        |                  |
 *                        |                  |
 *                      ------ -------------------
 * http://localhost:1337/start?foo=bar&hello=world
 *                             ---           -----
 *                              |              |
 *                              |              |
 *           querystring(string)["foo"]        |
 *                                             |
 *                          querystring(string)["hello"]
 */
var url = require('url');

function start(route, handle) {

    var server = http.createServer(function(request, response) {

        var pathname = url.parse(request.url).pathname;

        console.log("Request for " + pathname + " received.");

        route(handle, pathname, response);
    });
    server.listen(1337, '127.0.0.1');

    console.log("Server has started.");
}

exports.start = start;
