/**
 * request_handler.js
 */

var fs = require('fs');

var responder = require('./responder');

function foo(response) {
    console.log("Request handler ´foo´ was called.");
    responder.respond(response, 200, {'content': "Hello Foo" });
}

function bar(response) {
    console.log("Request handler ´bar´ was called.");

    /**
     *
     * Nej! As the saying goes:
     *   ´´In node, everything runs in parallel, except your code´´
     *
     * (function() {
     *     var startTime = new Date().getTime();
     *     while (new Date().getTime() < startTime + 1e4);
     * })();
     *
     */

    setTimeout(function() {
        responder.respond(response, 200, {'content': "Hello Bar" });
    }, 1e4);
}

exports.foo = foo;
exports.bar = bar;
