/**
 * server.js
 */

var http = require('http');

/**
 * Module ´url´
 *                               url.parse(string).query
 *                                           |
 *            url.parse(string).pathname     |
 *                        |                  |
 *                        |                  |
 *                      ------ -------------------
 * http://localhost:1337/start?foo=bar&hello=world
 *                             ---           -----
 *                              |              |
 *                              |              |
 *           querystring(string)["foo"]        |
 *                                             |
 *                          querystring(string)["hello"]
 */
var url = require('url');
var querystring = require('querystring');

function start(route, handle) {

    var server = http.createServer(function(request, response) {

        var pathname = url.parse(request.url).pathname;
        console.log("Request for " + pathname + " received.");

        var query = url.parse(request.url).query;
        var data = {};

        if (query) {
            data['query'] = querystring.parse(query);
            console.error('query: ' + data['query']);
        }

        if (request.method == 'POST') {
            var body = '';

            request.on('data', function(dataMsg) {
                body += dataMsg;
            });

            request.on('end', function () {
                data['body'] = querystring.parse(body);
                route(handle, pathname, data, response);
            });

        } else {
            route(handle, pathname, data, response);
        }
    });
    server.listen(1337, '127.0.0.1');

    console.log("Server has started.");
}

exports.start = start;
