/**
 * request_handler.js
 */

function foo() {
    console.log("Request handler ´foo´ was called.");
    return "Hello Foo";
}

function bar() {
    console.log("Request handler ´bar´ was called.");

    /**
     *
     * Nej! As the saying goes:
     *   ´´In node, everything runs in parallel, except your code´´
     *
     * (function() {
     *     var startTime = new Date().getTime();
     *     while (new Date().getTime() < startTime + 1e4);
     * })();
     *
    */

    return "Hello Bar";
}

exports.foo = foo;
exports.bar = bar;
