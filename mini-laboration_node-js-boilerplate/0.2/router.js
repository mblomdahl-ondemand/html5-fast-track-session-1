var request_handler = require('./request_handler');

function route(handle, pathname) {
    console.log("Route initiated for " + pathname);
    if (typeof handle[pathname] === 'function') {
        return handle[pathname]();
    } else {
        console.log("No request handler found for " + pathname);
        return "404 Not Found";
    }
}

exports.route = route;
