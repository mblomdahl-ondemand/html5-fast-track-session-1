/**
 * server.js
 */

var http = require('http');

/**
 * Module ´url´
 *                               url.parse(string).query
 *                                           |
 *            url.parse(string).pathname     |
 *                        |                  |
 *                        |                  |
 *                      ------ -------------------
 * http://localhost:1337/start?foo=bar&hello=world
 *                             ---           -----
 *                              |              |
 *                              |              |
 *           querystring(string)["foo"]        |
 *                                             |
 *                          querystring(string)["hello"]
 */
var url = require('url');

function start(route, handle) {

    /**
     *
     * Nej! Tänka om från C-konvention.
     */
    function onRequest(request, response) {
        var pathname = url.parse(request.url).pathname;

        console.log("Request for " + pathname + " received.");

        var content = route(handle, pathname);
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.write(content);
        response.end();

        console.log("Response served.");
    }

    /**
     *
     * Nej! Tänka om från C-konvention.
     */
    var server = http.createServer(onRequest);
    server.listen(1337, '127.0.0.1');

    console.log("Server has started.");
}

exports.start = start;
