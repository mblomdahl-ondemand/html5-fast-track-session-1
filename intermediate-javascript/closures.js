/**
 * closures.js
 */

/**
 * Closures are an extension of the concept of scope. With closures, functions have access to variables that were
 * available in the scope where the function was created.
 */

// Each function executed within the loop will reference
// the last value stored in i (5).
// This won't behave as we want it to - every 100 milliseconds, 5 will alert
for ( var i = 0; i < 5; i++ ) {
    setTimeout(function() {
        alert( i );
    }, i * 100 );
}






/**
 * Closures can be used to prevent this by creating a unique scope for each iteration – storing each unique value of
 * the variable within its scope.
 */

// Using a closure to create a new private scope
// fix: “close” the value of i inside createFunction, so it won't change
var createFunction = function( i ) {
    return function() {
        alert( i );
    };
};

for ( var i = 0; i < 5; i++ ) {
    setTimeout( createFunction( i ), i * 100 );
}






/**
 * Closures can also be used to resolve issues with the this keyword, which is unique to each scope:
 */

// Using a closure to access inner and outer object instances simultaneously.
var outerObj = {
    myName: "outer",
    outerFunction: function() {
        // provide a reference to outerObj through innerFunction's closure
        var self = this;
        var innerObj = {
            myName: "inner",
            innerFunction: function() {
                console.log( self.myName, this.myName ); // "outer inner"
            }
        };

        innerObj.innerFunction();

        console.log( this.myName ); // "outer"
    }
};

outerObj.outerFunction();







/**
 * Closures can be particularly useful when dealing with callbacks. However, it is often better to use Function.bind,
 * which will avoid any overhead associated with scope traversal.
 *
 * Function.bind is used to create a new function. When called, the new function then calls itself in the context of
 * the supplied ´´this´´ value, using a given set of arguments that will precede any arguments provided when the new
 * function was initially called.
 *
 * One of the simplest uses of .bind() is making a function that is called with a particular value for ´´this´´,
 * regardless of how it's called. A common mistake developers make is attempting to extract a method from an object,
 * then later calling that method and expecting it to the use the origin object as its this. However, this can be
 * solved by creating a bound function using the original object as demonstrated below:
 */

// Let's manipulate ´´this´´ with a basic example.
var user = "johnsmith";
var module = {
    getUser: function() {
        return this.user;
    },
    user: "janedoe"
};

// module.getUser() is called where ´´module´´ is ´´this´´ and ´´module.user´´ is returned.

// janedoe
module.getUser();

// let's now store a reference in the global version of ´´this´´
var getUser = module.getUser;

// getUser() called, ´´this´´ is global, ´´user´´ is returned

// johnsmith
getUser();

// store a ref with ´´module´´ bound as ´´this´´
var boundGetUser = getUser.bind( module );

// boundGetUser() called, ´´module´´ is ´´this´´ again, ´´module.user´´ returned.

// janedoe
boundGetUser();



