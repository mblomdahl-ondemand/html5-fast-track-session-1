/**
 * functions.js
 *
 * Source: http://learn.jquery.com/javascript-101/functions/
 */

/**
 * Functions as Arguments: In JavaScript, functions are "first-class citizens" – they can be assigned to variables or
 * passed to other functions as arguments. Passing functions as arguments is an extremely common idiom in jQuery.
 */





// Passing an anonymous function as an argument
var myFn = function( fn ) {
    var result = fn();
    console.log( result );
};

myFn( function() { // logs "hello world"
    return "hello world";
});






// Passing a named function as an argument
var myFn = function( fn ) {
    var result = fn();
    console.log( result );
};

var myOtherFn = function() {
    return "hello world";
};

myFn( myOtherFn ); // logs "hello world"
