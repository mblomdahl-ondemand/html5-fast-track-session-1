/**
 * array-methods.js
 */

// Length of an array
var myArray = [ "hello", "world", "!" ];

console.log( myArray.length ); // 3





// For loops and arrays - a classic
var myArray = [ "hello", "world", "!" ];

for ( var i = 0; i < myArray.length; i = i + 1 ) {
    console.log( myArray[i] );
}





// For loops and arrays - alternate method
var myArray = [ "hello", "world", "!" ];

for ( var i in myArray ) {
    console.log( myArray[ i ] );
}





// Concatenating Arrays
var myArray = [ 2, 3, 4 ];
var myOtherArray = [ 5, 6, 7 ];

// [ 2, 3, 4, 5, 6, 7 ]
var wholeArray = myArray.concat( myOtherArray );







// Joining elements
var myArray = [ "hello", "world", "!" ];

// The default separator is a comma
console.log( myArray.join() );     // "hello,world,!"

// Any string can be used as separator...
console.log( myArray.join(" ") );  // "hello world !";
console.log( myArray.join("!!") ); // "hello!!world!!!";

// ...including an empty one
console.log( myArray.join("") );   // "helloworld!"






// pushing and popping
var myArray = [];

myArray.push( 0 ); // [ 0 ]
myArray.push( 2 ); // [ 0 , 2 ]
myArray.push( 7 ); // [ 0 , 2 , 7 ]
myArray.pop();     // [ 0 , 2 ]




// reverse
var myArray = [ "world" , "hello" ];

// [ "hello", "world" ]
myArray.reverse();






// queue with shift() and push()
var myArray = [];

myArray.push( 0 ); // [ 0 ]
myArray.push( 2 ); // [ 0 , 2 ]
myArray.push( 7 ); // [ 0 , 2 , 7 ]
myArray.shift();   // [ 2 , 7 ]






// slicing
var myArray = [ 1, 2, 3, 4, 5, 6, 7, 8 ];
var newArray = myArray.slice( 3 );

console.log( myArray );  // [ 1, 2, 3, 4, 5, 6, 7, 8 ]
console.log( newArray ); // [ 4, 5, 6, 7, 8 ]







// splice method
myArray.splice( index, length, values /* , ... */ );

// splice example
var myArray = [ 0, 7, 8, 5 ];
myArray.splice( 1, 2, 1, 2, 3, 4 );

console.log( myArray ); // [ 0, 1, 2, 3, 4, 5 ]






// sorting without comparing function
var myArray = [ 3, 4, 6, 1 ];

myArray.sort(); // 1, 3, 4, 6

// sorting with comparing function
function descending( a, b ) {
    return b - a; // lt(0): ´a´ before ´b´, gt(0): ´b´ before ´a´
}

var myArray = [ 3, 4, 6, 1 ];

myArray.sort( descending ); // [ 6, 4, 3, 1 ]







// native forEach
function printElement( elem ) {
    console.log( elem );
}

function printElementAndIndex( elem, index ) {
    console.log( "Index " + index + ": " + elem );
}

function negateElement( elem, index, array ) {
    array[ index ] = -elem;
}

myArray = [ 1, 2, 3, 4, 5 ];

// prints all elements to the console
myArray.forEach( printElement );

// prints "Index 0: 1" "Index 1: 2" "Index 2: 3" ...
myArray.forEach( printElementAndIndex );

// myArray is now [ -1, -2, -3, -4, -5 ]
myArray.forEach( negateElement );





