/**
 * loops.js
 */

// A for loop
// logs "try 0", "try 1", ..., "try 4"
for ( var i = 0; i < 5; i++ ) {

    console.log( "try " + i );

}






// A typical while loop
var i = 0;
while ( i < 100 ) {
    // This block will be executed 100 times
    console.log( "Currently at " + i );

    // increment i
    i++;
}





// A while loop with a combined conditional and incrementer
var i = -1;
while ( ++i < 100 ) {
    // This block will be executed 100 times
    console.log( "Currently at " + i );
}






do {
    // Even though the condition evaluates to false
    // this loop's body will still execute once.
    alert( "Hi there!" );

} while ( false );
