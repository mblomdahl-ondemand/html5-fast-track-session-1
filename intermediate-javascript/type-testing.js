/**
 * type-testing.js
 */

// Testing the type of various variables.
var myFunction = function() {
    console.log( "hello" );
};
var myObject = {
    foo: "bar"
};
var myArray = [ "a", "b", "c" ];
var myString = "hello";
var myNumber = 3;

typeof myFunction; // "function"
typeof myObject;   // "object"
typeof myArray;    // "object" -- Careful!
typeof myString;   // "string"
typeof myNumber;   // "number"
typeof null;       // "object" -- Careful!
