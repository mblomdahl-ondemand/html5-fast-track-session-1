/**
 * truthy-falsy.js
 */

// Values that evaluate to true
"0";
"any string";
[]; // an empty array
{}; // an empty object
1;  // any non-zero number







// Values that evaluate to false
"";  // an empty string
NaN; // JavaScript's "not-a-number" variable
null;
undefined; // be careful -- undefined can be redefined!
0; // the number zero







// Comparison operators
var foo = 1;
var bar = 0;
var baz = "1";
var bim = 2;

foo == bar; // false
foo != bar; // true
foo == baz; // true; but note that the types are different

foo === baz;             // false
foo !== baz;             // true
foo === parseInt( baz ); // true

