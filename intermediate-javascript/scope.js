/**
 * scope.js
 *
 * Source: http://learn.jquery.com/javascript-101/scope/
 */

/**
 * "Scope" refers to the variables that are available to a piece of code at a given time. A lack of understanding of
 * scope can lead to frustrating debugging experiences. The idea of "scope" is that it's where certain functions or
 * variables are accessible from in our code, and the context in which they exist and are executed in.
 *
 * There are two types of scopes in JavaScript: global and local. Let's talk about each of them in turn.
 */

var x = 9; // Global Scope (window.x)






function myFunc() {
    var x = 5; // Local Scope
}
console.log( x ); // ReferenceError: x is not defined






(function() { // Immediately-Invoked Function Expression
    var jQuery = { /* all my methods go here */ };
    window.jQuery = jQuery;
})();






function outer() {
    var x = 5;

    function inner() {
        console.log( x );
        var y = 10;
    }

    inner(); //  5

    console.log( y ); // ReferenceError: y is not defined
}





(function() {

    var baz = 1;

    var bim = function() {
        console.log( baz );
    };

    bar = function() {
        console.log( baz );
    };

})();

console.log( baz ); // baz is not defined outside of the function

bar(); //  1

bim(); // ReferenceError: bim is not defined






