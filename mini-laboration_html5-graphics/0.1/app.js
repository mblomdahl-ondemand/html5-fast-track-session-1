/**
 * app.js
 */

var server = require('./server');

var router = require('./router');

var request_handler = require('./request_handler');

var handle = {};
handle['/'] = request_handler.foo;
handle['/foo'] = request_handler.foo;
handle['/bar'] = request_handler.bar;

server.start(router.route, handle);
